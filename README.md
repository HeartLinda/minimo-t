[![GitHub release](https://img.shields.io/github/release/MunifTanjim/minimo.svg?style=for-the-badge)](https://github.com/MunifTanjim/minimo/releases/latest)
[![GitHub Release Date](https://img.shields.io/github/release-date/MunifTanjim/minimo.svg?style=for-the-badge)](https://github.com/MunifTanjim/minimo/releases)
[![license](https://img.shields.io/github/license/MunifTanjim/minimo.svg?style=for-the-badge)](https://github.com/MunifTanjim/minimo/blob/master/LICENSE)

![Minimo – Minimalist theme for Hugo](https://raw.githubusercontent.com/MunifTanjim/minimo/master/images/tn.png)

# Minimo

Minimalist theme for Hugo.

## About This Fork

This is a fork by HeartLinda which includes some customizations to mimic Tumblr's nagivation. The primary change is to include the entire page contents in a taxonomy list as well as on the homepage. While originally developed for a Tumblr blog mirror, this fork can be used for any blog to provide immediate access to post content.

The [original theme](https://github.com/MunifTanjim/minimo) is on GitHub. The `master` branch in the upstream is `upstream` here.

## Documentation

Check the [Minimo Documentation](https://minimo.netlify.com/docs/) for detailed documentation of Minimo.

#### Getting Up & Running

Follow these guides for getting your site up & running with Minimo:

- **Install Minimo**: [Installation Guide](https://minimo.netlify.com/docs/installation)
- **Setup Authors**: [Authors Setup Guide](https://minimo.netlify.com/docs/authors)
- **Configure Widgets**: [Widgets Documentation](https://minimo.netlify.com/docs/widgets)

#### Updating Minimo

Follow the [**Updating Guide**](https://minimo.netlify.com/docs/updating) to update Minimo to its latest version.

After updating Minimo, always check that your site's **`config.toml`** file matches the latest [**`config.toml`** file](https://minimo.netlify.com/docs/config-file) format.

A good idea is to double check all the [Configuration settings](https://minimo.netlify.com/docs/installation#configuration-for-minimo) of Minimo.

## Development

If you find a bug or want to request a new feature, feel free to open an issue.

## Changelog

[Changelog for Minimo](https://github.com/MunifTanjim/minimo/blob/master/CHANGELOG.md)

## License

Minimo is licensed under the MIT License. Check the [LICENSE](https://github.com/MunifTanjim/minimo/blob/master/LICENSE) file for details.

The following resources are included/used in the theme:

- [Feather](https://feather.netlify.com/) by Cole Bemis - Licensed under the [MIT License](https://github.com/colebemis/feather/blob/master/LICENSE).
